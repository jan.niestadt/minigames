const DIRECTIONS = [
    'oost',
    'west',
    'noord',
    'zuid',
    'binnen',
    'buiten',
    'omhoog',
    'omlaag'
];

let engine;

class Parser {

    SYNONYMS = {
        'o': 'oost',
        'w': 'west',
        'n': 'noord',
        'z': 'zuid',
    }
    
    constructor() {
        this.stopWords = [];
    }

    addStopWords(l) {
        for (let w of l) {
            this.stopWords.push(w);
        }
    }

    removeStopWords(l) {
        return l.filter( w => this.stopWords.indexOf(w) < 0);
    }
    
    parse(command) {
        const words = command.split(/\s+/g);
        const restUnfiltered = words.slice(1);
        let rest = engine.parser.removeStopWords(restUnfiltered);
        let verb = this.applySynonyms(words[0]);
        if (DIRECTIONS.indexOf(verb) >= 0) {
            rest = [verb, ...rest];
            verb = 'ga';
        }
        const verbFull = words[0].length === 1 ? verb : words[0];
        return {
            words,
            rest,
            verb,
            verbFull
        };
    }

    applySynonyms(w) {
        if (w in this.SYNONYMS)
            return this.SYNONYMS[w];

        for (let verb in COMMANDS) {
            const command = COMMANDS[verb];
            if ('synonyms' in command && command.synonyms.indexOf(w) >= 0) {
                return verb;
            }
        }

        return w;
    }
}

class Locations {

    constructor(engine) {
        this.engine = engine;
        this.locations = [];
    }

    [Symbol.iterator]() {
        var index = -1;

        return {
            next: () => ({ value: this.locations[++index], done: index >= this.locations.length })
        };
    }

    get length() {
        return this.locations.length;
    }

    find(pred) {
        return this.locations.find(pred);
    }

    add(l) {
        if (typeof l === 'string' || !l.length)
            l = [l];
        for (let loc of l) {
            this.locations.push(loc);
            loc.objects = new Objects(this.engine);
            if (loc.scenery) {
                loc.objects.add(loc.scenery);
            }
            if (loc.props)
                loc.objects.add(loc.props, true);
        }
    }

    resolveAllObjects() {
        for (let loc of this.locations) {
            loc.objects.resolveAll();
        }
    }

    get all() {
        return this.locations;
    }

    get(loc) {
        // Make sure we have an object structure
        if (typeof loc === 'string')
            loc = this.locations.find(l => l.name === loc) || undefined;
        return loc;
    }

    // (Safely) call a handler for each object in a location (or player's inventory)
    forEachObject(location, handler, f = (o) => true) {
        location = this.get(location);
        const objs = [...location.objects]; // copy so we can remove objects
        objs.filter(f).forEach(handler);
    }

}

class Objects {

    constructor(engine) {
        this.engine = engine;
        this.objects = [];
    }

    [Symbol.iterator]() {
        var index = -1;

        return {
            next: () => ({ value: this.objects[++index], done: index >= this.objects.length })
        };
    }

    get length() {
        return this.objects.length;
    }

    find(pred) {
        return this.objects.find(pred);
    }

    map(func) {
        return this.objects.map(func);
    }

    filter(func) {
        return this.objects.filter(func);
    }

    get props() {
        return this.objects.filter(o => o.isProp);
    }

    get scenery() {
        return this.objects.filter(o => !o.isProp);
    }

    add(l, addAsProps = false) {
        if (typeof l === 'string' || !l.length)
            l = [l];
        for (let obj of l) {
            if (typeof obj === 'string') {
                obj = {
                    name: obj,
                    placeholder: true,
                };
            } else {
                obj = {
                    article: 'de',
                    ...obj
                };
            }
            if (addAsProps)
                obj.isProp = true;
            const i = this.objects.findIndex(o => o.name === obj.name && o.placeholder);
            if (i >= 0)
                this.objects.splice(i, 1);
            this.objects.push(obj);
            if (!obj.placeholder)
                this.engine.onAddObject(obj);
        }
    }

    remove(o) {
        o = this.get(o);
        const i = this.objects.indexOf(o);
        if (i >= 0)
            this.objects.splice(i, 1);
    }

    resolveAll() {
        const objCopy = this.objects.slice();
        for (let obj of objCopy) {
            if (obj.placeholder) {
                const r = this.engine.objects.get(obj.name);
                this.add([r], obj.isProp);
            }
        }
    }

    get all() {
        return this.objects;
    }

    get(obj) {
        // Make sure we have an object structure
        if (typeof obj === 'string') {
            for (let o of this.objects) {
                if (o.name === obj)
                    return o;
            }
            return { name: obj, notFound: true };
        }
        return obj;
    }
}

class Engine {

    constructor() {
        this.text = {};
        this.parser = new Parser();
        this.locations = new Locations(this);
        this.objects = new Objects(this);

        this.player = undefined;
        this.playerLocation = undefined;

        engine = this;
    }

    addText(text) {
        this.text = { ...this.text, ...text };
    }

    getParser() {
        return this.parser;
    }

    get t() {
        return this.text;
    }

    onAddObject(obj) {
        if (obj.verbs) {
            for (let v in obj.verbs) {
                COMMANDS[v] = COMMANDS[v] || ((w) => {
                    if (w.length > 0) {
                        const object = this.findByName(w[0]);
                        if (object)
                            print(this.t.VERB_DOES_NOT_APPLY);
                        else
                            print(engine.objectString(w[0], engine.t.OBJECT_NOT_HERE));
                    } else
                        print(this.t.VERB_DOES_NOT_APPLY);
                });
            }
        }
    }

    destroyObject(obj) {
        const loc = this.locations.find(l => l.objects.find(o => o.name === obj.name));
        if (loc) {
            loc.objects.remove(obj);
        }
    }

    createPlayer(location, inventory) {
        this.playerLocation = this.locations.get(location);
        this.locations.add([{
            name: 'player',
            description: 'The player.',
            props: inventory
        }]);
        this.player = this.locations.get('player');
    }
    
    start() {
        initUi(this);

        this.locations.resolveAllObjects();
        //this.objects = null;

        this.describeLocation();
    }

    executeCommand(command) {
        const parsed = this.parser.parse(command);
        printHtml(`<p class='command'>&gt; ${parsed.verbFull} ${parsed.words.slice(1).join(" ")}</p>`);
    
        const object = parsed.rest[0] && this.findByName(parsed.rest[0]);
        if (object && object.verbs && parsed.verb in object.verbs) {
            // Specific verb for object.
            object.verbs[parsed.verb](object, parsed.rest.slice(1));
        } else if (parsed.verb in COMMANDS) {
            // Generic verb?
            const command = COMMANDS[parsed.verb];
            if ('handler' in command) {
                command.handler(parsed.rest);
            } else
                command(parsed.rest); // legacy
        } else if (DIRECTIONS.indexOf(parsed.verb) >= 0) {
            // Direction without 'go'?
            COMMANDS.ga([parsed.verb, ...parsed.rest]);
        } else {
            print(`Ik begrijp '${parsed.verbFull}' niet.`);
        }
    }
    
    describeLocation() {
        print(this.playerLocation.description);
        if (this.playerLocation.objects.props.length > 0) {
            print(`Je ziet hier ${this.listObjectsInline(this.playerLocation.objects.props)}.`);
        }
    }

    articleObject(object, definiteArticle, longDescription = true) {
        const article = definiteArticle ? object.article : this.t.INDEFINITE_ARTICLE;
        return object.title || `${article} ${longDescription && object.adjectives ? `${object.adjectives} ` : ''}${object.name}`;
    }
    
    anObject(object, longDescription = true) {
        return this.articleObject(object, false, longDescription);
    }
    
    theObject(object, longDescription = true) {
        return this.articleObject(object, true, longDescription);
    }
    
    objectString(obj, str) {
        obj = typeof obj === 'string' ? { name: obj } : obj; //engine.objects.get(obj);
        return str.replace(/{\w+}/g, (t) => {
            switch (t) {
            case "{OBJECT}": return `${obj.adjectives ? `${obj.adjectives} ` : ''}${obj.name}`;
            case "{OBJECT_SHORT}": return obj.name;
            case "{THE_OBJECT}": return engine.theObject(obj);
            case "{THE_OBJECT_SHORT}": return engine.theObject(obj, false);
            case "{AN_OBJECT}": return engine.anObject(obj);
            case "{AN_OBJECT_SHORT}": return engine.anObject(obj, false);
            }
        });
    }

    listInline(parts) {
        return parts.join(", ").replace(/,([^,]+)$/, " en $1");
    }
    
    listObjectsInline(objects) {
        if (objects.length > 0) {
            let parts = [];
            for (let object of objects) {
                parts.push(engine.anObject(object));
            }
            return this.listInline(parts);
        }
    }

    listHtml(l) {
        if (l.length > 0) {
            let parts = [];
            parts.push("<ul>");
            for (let name of l) {
                parts.push(`<li>${name}</li>`);
            }
            parts.push("</ul>");
            return(parts.join(""));
        }
    }
    
    listObjects(objects) {
        if (objects.length > 0) {
            printHtml(this.listHtml(objects.map(name => engine.anObject(name))));
        }
    }

    findByName(name, includeLocation = true, includeInventory = true, propsOnly = true) {
        //const obj = engine.objects.get(name);
        //console.log('zoek', obj);
    
        // See if the object is here (in location or inventory)
        if (includeInventory) {
            for (let object of engine.player.objects) {
                console.log('inv', object);
                if (object.name === name && (object.isProp || !propsOnly))
                    return object;
            }
        }
        if (includeLocation) {
            for (let object of engine.playerLocation.objects) {
                console.log('loc', object);
                if (object.name === name && (object.isProp || !propsOnly))
                    return object;
            }
        }
    
        return undefined;
    }
    
    findInInventoryByName(name, propsOnly = true) {
        return this.findByName(name, false, true, propsOnly);
    }
    
    findOnLocationByName(name, propsOnly = true) {
        return this.findByName(name, true, false, propsOnly);
    }
}

const COMMANDS = {

    //----------------------------------------------------------------
    kijk: {
        handler: (w) => {
            if (w.length === 0) {
                // Kijk rond
                engine.describeLocation();
            } else {
                // Bekijk iets
                const object = engine.findByName(w[0]);
                if (object) {
                    print(object.description || engine.t.OBJECT_WITHOUT_DESCRIPTION);
                } else {
                    print(engine.objectString(w[0], engine.t.OBJECT_NOT_HERE));
                }
            }
        },
        synonyms: ['k', 'bekijk'],
    },

    //----------------------------------------------------------------
    ga: {
        handler: (w) => {
            let i = 0;
            const stemmed = w[i].replace(/en$/, '');
            const match = stemmed in engine.playerLocation.connections ? stemmed : (w[i] in engine.playerLocation.connections ? w[i] : undefined);
            if (match) {
                const naam = engine.playerLocation.connections[match];
                engine.playerLocation = engine.locations.get(naam);
                engine.describeLocation();
            } else {
                print(engine.t.CANNOT_GO_THERE);
            }
        },
        synonyms: ['loop', 'wandel', 'ren', 'beweeg', 'verplaats'],
    },

    //----------------------------------------------------------------
    pak: {
        handler: (w) => {
            const name = w[0];
            if (name === 'alles') {
                engine.locations.forEachObject(engine.playerLocation, (o) => COMMANDS.pak.handler([o.name]),
                o => o.isProp);
            } else {
                const object = engine.findOnLocationByName(name);
                if (object) {
                    engine.playerLocation.objects.remove(object);
                    engine.player.objects.add(object);
                    print(engine.objectString(object, engine.t.YOU_TAKE_OBJECT));
                } else {
                    print(engine.objectString({ name }, engine.t.OBJECT_NOT_HERE));
                }
            }
        },
        synonyms: ['neem'],
    },

    //----------------------------------------------------------------
    drop: {
        handler: (w) => {
            const name = w[0];
            if (name === 'alles') {
                engine.locations.forEachObject(engine.player, (o) => COMMANDS.drop.handler([o.name]));
            } else {
                const object = engine.findInInventoryByName(name);
                if (object) {
                    engine.player.objects.remove(object);
                    engine.playerLocation.objects.add(object);
                    print(engine.objectString(object, engine.t.YOU_DROP_OBJECT));
                } else {
                    print(engine.objectString({ name }, engine.t.YOU_DO_NOT_HAVE_OBJECT));
                }
            }
        },
        synonyms: ['leg'],
    },

    //----------------------------------------------------------------
    inventaris: {
        handler: (w) => {
            if (engine.player.objects.length > 0) {
                print(`${engine.t.YOU_OWN}:`);
                engine.listObjects(engine.player.objects);
            } else {
                print(engine.t.YOU_OWN_NOTHING);
            }
        },
        synonyms: ['i', 'bezit', 'voorwerpen'],
    },

    //----------------------------------------------------------------
    teleport: {
        handler: (w) => {
            if (w[0] in engine.locations.all) {
                engine.playerLocation = engine.locations.get(w[0]);
                engine.describeLocation();
            } else {
                print("Locatie niet gevonden.");
            }
        },
    }
};
