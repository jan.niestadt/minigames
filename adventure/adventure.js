document.addEventListener("DOMContentLoaded", () => {

    const engine = new Engine();

    //====================================================================
    engine.addText({
        INDEFINITE_ARTICLE: "een",
    
        YOU_TAKE_OBJECT: "Je pakt {THE_OBJECT_SHORT} op.",
        CANNOT_TAKE_OBJECT: "Je kunt {THE_OBJECT_SHORT} niet pakken.",
        YOU_DO_NOT_HAVE_OBJECT: "Je hebt geen {OBJECT_SHORT}.",
        YOU_DROP_OBJECT: "Je laat {THE_OBJECT_SHORT} achter.",

        OBJECT_NOT_HERE: "Ik zie hier geen {OBJECT_SHORT}.",
        OBJECT_WITHOUT_DESCRIPTION: 'Onbeschrijfelijk!',

        CANNOT_GO_THERE: "Daar kun je niet heen vanaf hier.",
        
        YOU_OWN: "Je bezit",
        YOU_OWN_NOTHING: "Je bezit helemaal niets!",

        VERB_DOES_NOT_APPLY: "Dat gaat niet.",
    });

    //====================================================================

    engine.locations.add([
        {
            name: 'tuin',
            //--------------------------------------------------------
            description: 'Je bent in een tuin. Er is een gazon en wat planten. Naar het oosten is een huis.',
            scenery: [
                {
                    name: 'gazon',
                    description: 'Het is kort gemaaid en ziet er fris groen uit.',
                    synonyms: ['gras', 'grasveld'],
                },
                {
                    name: 'planten',
                    description: 'Je hebt er geen verstand van maar herkent een van de planten als een roos.',
                    synonyms: ['roos', 'struiken'],
                },
            ],
            props: [
                'bal'
            ],
            connections: {
                oost: 'huis',
                binnen: 'huis',
                huis: 'huis'
            }
        },

        {
            name: 'huis',
            //--------------------------------------------------------
            description: 'Je bent in een huis. Naar het westen is de voordeur.',
            props: [
                'kaas'
            ],
            connections: {
                west: 'tuin',
                buiten: 'tuin',
                voordeur: 'tuin'
            }
        }
    ]);

    //====================================================================
    engine.objects.add([
        {
            name: 'stofje',
            //--------------------------------------------------------
            description: 'Een pluizig stofje dat je in je broekzak vond.',
            article: 'het',
        },
        {
            name: 'bal',
            //--------------------------------------------------------
            adjectives: 'kleurige',
            description: 'De bal heeft allerlei vrolijke kleurtjes.',
            verbs: {
                gooi: (o, w) => {
                    print('Je gooit de bal omhoog en vangt \'m weer op. Whee!');
                }
            },
        },
        {
            name: 'kaas',
            //--------------------------------------------------------
            titleShort: ['de', 'kaas'],
            titleFull: ['het', 'stuk oude kaas'],
            article: 'het',
            adjectives: 'stuk oude',
            description: 'Een stuk brokkelige oude kaas.',
            verbs: {
                eet: (o, w) => {
                    print('Je eet de kaas op. Om nom nom! Hmm, heerlijk!');
                    engine.destroyObject(o);
                }
            },
        }
    ]);


    //====================================================================
    engine.createPlayer('tuin', [ 'stofje' ]);


    //====================================================================
    engine.getParser().addStopWords([
        // lidwoorden
        'de',
        'het',
        'een',

        // tussenwerpsels
        'en',
        'of',

        // voorzetsels
        'naar',
        'door',
        'in',
        'met',
        'op',
        'mee'
    ]);

    engine.start();

});
