let input, output;

function printHtml(html) {
    output.innerHTML += html;
    window.scrollTo(0, document.body.scrollHeight);
}

function print(txt) {
    printHtml(`<p>${txt}</p>`);
}

function initUi(engine) {
    document.getElementById('inputForm').addEventListener('submit', (e) => {
        e.preventDefault();
        const command = input.value;
        input.value = '';
        engine.executeCommand(command);
    });
    input = document.getElementById('input');
    input.focus();
    output = document.getElementById('output');
    document.addEventListener('click', () => { input.focus(); });
}
